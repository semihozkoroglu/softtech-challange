package com.softtechventures.challange

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Created by semihozkoroglu on 24.07.2021.
 */
@HiltAndroidApp
class App : Application() {
}