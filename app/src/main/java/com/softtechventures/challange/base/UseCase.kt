package com.softtechventures.challange.base

import kotlinx.coroutines.flow.Flow

/**
 * Created by semihozkoroglu on 24.07.2021.
 */
abstract class UseCase<P, R> {
    operator fun invoke(params: P): Flow<R> = execute(params)

    protected abstract fun execute(params: P): Flow<R>
}