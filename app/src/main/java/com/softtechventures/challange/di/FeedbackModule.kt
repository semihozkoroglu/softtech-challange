package com.softtechventures.challange.di

import android.content.Context
import com.softtechventures.challange.repository.feedback.FeedbackService
import com.softtechventures.challange.repository.feedback.MockFeedbackService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * Created by semihozkoroglu on 24.07.2021.
 */
@Module
@InstallIn(SingletonComponent::class)
class FeedbackModule {

    @Singleton
    @Provides
    fun provideFeedbackService(@ApplicationContext context: Context): FeedbackService = MockFeedbackService(context)
}