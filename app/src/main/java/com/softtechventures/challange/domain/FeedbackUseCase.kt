package com.softtechventures.challange.domain

import com.softtechventures.challange.base.UseCase
import com.softtechventures.challange.domain.model.FeedbackUI
import com.softtechventures.challange.repository.feedback.FeedbackService
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

/**
 * Created by semihozkoroglu on 24.07.2021.
 */
class FeedbackUseCase @Inject constructor(val service: FeedbackService) :
    UseCase<Unit, List<FeedbackUI>>() {

    override fun execute(params: Unit): Flow<List<FeedbackUI>> = service.getUserFeedback().map {
        val feedbacks = arrayListOf<FeedbackUI>()

        it.items?.forEach {
            feedbacks.add(FeedbackUI().apply {
                title = "${it.browserInfo?.browser} ${it.browserInfo?.version}"
                description = "${it.browserInfo?.platform}"
                location = "${it.country} ${it.coordinate?.lat}, ${it.coordinate?.lon}"
                rating = it.rating
                label = "${it.labels?.joinToString { "#$it " }}"
            })
        }

        feedbacks
    }
}