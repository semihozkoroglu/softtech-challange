package com.softtechventures.challange.domain.model

/**
 * Created by semihozkoroglu on 24.07.2021.
 */
class FeedbackUI {
    var title: String = ""
    var description: String = ""
    var location: String = ""
    var rating: Int? = 0
    var label: String = ""
}