package com.softtechventures.challange.repository.feedback

import com.softtechventures.challange.repository.feedback.model.FeedbackResponse
import kotlinx.coroutines.flow.Flow

/**
 * Created by semihozkoroglu on 24.07.2021.
 */
interface FeedbackService {

    fun getUserFeedback(): Flow<FeedbackResponse>
}