package com.softtechventures.challange.repository.feedback

import android.content.Context
import com.softtechventures.challange.R
import com.softtechventures.challange.repository.feedback.model.FeedbackResponse
import com.softtechventures.challange.util.extensions.getJson
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

/**
 * Created by semihozkoroglu on 24.07.2021.
 */
class MockFeedbackService(val context: Context) : FeedbackService {

    override fun getUserFeedback(): Flow<FeedbackResponse> = send {
        delay(2000)

        context.getJson(R.raw.feedback_response)
    }

    private fun <T> send(req: suspend () -> T): Flow<T> = flow { emit(req()) }
}