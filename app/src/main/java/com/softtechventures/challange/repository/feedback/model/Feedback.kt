package com.softtechventures.challange.repository.feedback.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

/**
 * Created by semihozkoroglu on 24.07.2021.
 */
@Parcelize
data class FeedbackResponse(
    val items: List<Feedback>?
) : Parcelable

@Parcelize
data class Feedback(
    @SerializedName("computed_browser")
    val browserInfo: BrowserInfo?,
    @SerializedName("computed_location")
    val country: String?,
    @SerializedName("geo")
    val coordinate: GeoInfo?,
    val rating: Int?,
    val labels: List<String>?
) : Parcelable

@Parcelize
data class BrowserInfo(
    @SerializedName("Browser")
    val browser: String?,
    @SerializedName("Version")
    val version: String?,
    @SerializedName("Platform")
    val platform: String?,
) : Parcelable

@Parcelize
data class GeoInfo(
    val country: String?,
    val region: String?,
    val city: String?,
    val lat: Double?,
    val lon: Double?
) : Parcelable