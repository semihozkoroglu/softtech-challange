package com.softtechventures.challange.ui.home

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.softtechventures.challange.R
import dagger.hilt.android.AndroidEntryPoint

/**
 * Created by semihozkoroglu on 24.07.2021.
 */
@AndroidEntryPoint
class ACHome : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.ac_home)
    }
}