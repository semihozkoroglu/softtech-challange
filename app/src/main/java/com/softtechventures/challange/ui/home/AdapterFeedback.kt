package com.softtechventures.challange.ui.home

import com.softtechventures.challange.R
import com.softtechventures.challange.domain.model.FeedbackUI
import com.softtechventures.challange.repository.feedback.model.Feedback
import com.softtechventures.challange.util.adapter.BaseAdapter
import javax.inject.Inject

/**
 * Created by semihozkoroglu on 24.07.2021.
 */
class AdapterFeedback @Inject constructor() : BaseAdapter<FeedbackUI>() {

    override fun getLayoutId(viewType: Int): Int {
        return R.layout.item_feedback
    }
}