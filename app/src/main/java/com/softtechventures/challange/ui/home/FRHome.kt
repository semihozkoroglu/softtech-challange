package com.softtechventures.challange.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.softtechventures.challange.R
import com.softtechventures.challange.databinding.FrHomeBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by semihozkoroglu on 24.07.2021.
 */
@AndroidEntryPoint
class FRHome : Fragment() {

    lateinit var vi: FrHomeBinding

    val viewModel: FRHomeVM by viewModels()

    @Inject
    lateinit var adapter: AdapterFeedback

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        vi = DataBindingUtil.inflate(inflater, R.layout.fr_home, container, false)

        return vi.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        vi.rvFeedback.adapter = adapter

        viewModel.getFeedback()

        lifecycleScope.launch {
            viewModel.uiAdapter.collect {
                it.let { adapter.submitList(it) }
            }
        }

        lifecycleScope.launch {
            viewModel.uiProgress.collect {
                vi.pbProgress.visibility = if (it) View.VISIBLE else View.GONE
            }
        }
    }
}