package com.softtechventures.challange.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.softtechventures.challange.domain.FeedbackUseCase
import com.softtechventures.challange.domain.model.FeedbackUI
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by semihozkoroglu on 24.07.2021.
 */
@HiltViewModel
class FRHomeVM @Inject constructor(val feedbackUseCase: FeedbackUseCase) : ViewModel() {

    private val _uiProgress = MutableStateFlow(true)
    val uiProgress: StateFlow<Boolean> = _uiProgress

    private val _uiAdapter = MutableStateFlow(emptyList<FeedbackUI>())
    val uiAdapter: StateFlow<List<FeedbackUI>> = _uiAdapter

    fun getFeedback() {
        viewModelScope.launch {
            feedbackUseCase(Unit)
                .onStart { _uiProgress.value = true }
                .onCompletion { _uiProgress.value = false }
                .collect {
                    _uiAdapter.value = it
                }
        }
    }
}