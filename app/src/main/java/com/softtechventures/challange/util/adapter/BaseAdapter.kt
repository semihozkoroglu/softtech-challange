package com.softtechventures.challange.util.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter

/**
 * Created by semihozkoroglu on 24.07.2021.
 */
abstract class BaseAdapter<T>(diffCallback: DiffUtil.ItemCallback<T> = EmptyDiffCallBack()) :
    ListAdapter<T, DataViewHolder<T>>(diffCallback) {

    abstract fun getLayoutId(viewType: Int): Int

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder<T> {
        return DataViewHolder(
            DataBindingUtil.inflate(
                LayoutInflater.from(parent.context),
                getLayoutId(viewType),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: DataViewHolder<T>, position: Int) {
        val item = getItem(position)
        holder.bind(item)
    }
}


class EmptyDiffCallBack<T> : DiffUtil.ItemCallback<T>() {
    override fun areItemsTheSame(oldItem: T, newItem: T): Boolean {
        return true
    }

    override fun areContentsTheSame(oldItem: T, newItem: T): Boolean {
        return true
    }
}