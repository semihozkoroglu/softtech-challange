package com.softtechventures.challange.util.adapter

import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.softtechventures.challange.BR

/**
 * Created by semihozkoroglu on 24.07.2021.
 */
open class DataViewHolder<T>(val binding: ViewDataBinding) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: T) {
        binding.setVariable(BR.item, item)
        binding.executePendingBindings()
    }
}