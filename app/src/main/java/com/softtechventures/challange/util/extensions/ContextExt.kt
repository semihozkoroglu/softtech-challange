package com.softtechventures.challange.util.extensions

import android.content.Context
import java.io.InputStream

/**
 * Created by semihozkoroglu on 24.07.2021.
 */
inline fun <reified T> Context.getJson(file: Int): T {
    val input: InputStream = resources.openRawResource(file)

    val b = ByteArray(input.available())
    input.read(b)

    return String(b).toModel()
}