package com.softtechventures.challange.util.extensions

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

/**
 * Created by semihozkoroglu on 24.07.2021.
 */
inline fun <reified T> String.toModel(): T = Gson().fromJson(this, object : TypeToken<T>() {}.type)